package com.axles.authtemp.common;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import javax.validation.constraints.NotNull;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

@Service
public class AmazonService {
    @Value("${amazon.endpointUrl}")
    private String endpointUrl;
    @Value("${amazon.bucketName}")
    private String bucketName;
    @Value("${amazon.region}")
    private String region;
    private AmazonS3 s3Client;

    public void deleteFileFromS3Bucket(String key) {
        try {
            s3Client.deleteObject(new DeleteObjectRequest(bucketName, key));
        } catch (AmazonServiceException ex) {
            ex.printStackTrace();
        }
    }

    @NotNull
    public String uploadFile(MultipartFile multipartFile, String folderName, String fileName) throws IOException, SdkClientException {
        String fileUrl = "";
        File file = convertToFile(multipartFile);
        String path = generateFilePath(folderName, fileName);
        fileUrl = endpointUrl + "/" + bucketName + "/" + path;
        uploadFileTos3bucket(path, file);
        file.delete();
        return fileUrl;
    }

    @PostConstruct
    private void initializeAmazon() {
        s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(region)
                .withCredentials(new EnvironmentVariableCredentialsProvider())
                .build();
    }

    private void uploadFileTos3bucket(String fileName, File file) throws SdkClientException {
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(file.length());
        metadata.setContentType("image/png");
        PutObjectRequest request = new PutObjectRequest(bucketName, fileName, file);
        request.setMetadata(metadata);
        s3Client.putObject(request);
    }

    private String generateFilePath(String folderName, String fileName) {
        return folderName + "/" + fileName;
    }

    private File convertToFile(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

}