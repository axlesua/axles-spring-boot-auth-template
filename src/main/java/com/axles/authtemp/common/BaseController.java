package com.axles.authtemp.common;

import com.axles.authtemp.users.data.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class BaseController {

    protected User getAuthenticatedUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

}
