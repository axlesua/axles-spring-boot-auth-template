package com.axles.authtemp.common;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ATErrorDto {
    private int code;
    private String message;
}
