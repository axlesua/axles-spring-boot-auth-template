package com.axles.authtemp.common;

public class Constants {
    public static class Headers {
        public static final String AUTHORIZATION = "Authorization";
    }
}
