package com.axles.authtemp.common;

import com.axles.authtemp.users.data.UserDto;
import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationUtils {
    private static final int passwordMinLength = 8;
    private static final int firstNameMinLength = 2;

    public static boolean isValidUserDto(UserDto userDto) {
        return userDto != null
                && isValidUsername(userDto.getUsername())
                && isValidPassword(userDto.getPassword())
                && isValidFirstName(userDto.getFirstName())
                && isValidLastName(userDto.getLastName());
    }

    public static boolean isValidUsername(String username) {
        return username != null && EmailValidator.getInstance().isValid(username);
    }

    public static boolean isValidPassword(String password) {
        return password != null && password.length() >= passwordMinLength;
    }

    public static boolean isValidFirstName(String firstName) {
        return firstName != null && firstName.length() >= firstNameMinLength;
    }

    public static boolean isValidLastName(String lastName) {
        return lastName != null && lastName.length() >= firstNameMinLength;
    }

    public static boolean isValidPhoneNumber(String number) {
        if ((number != null && !number.isEmpty())) {
            Pattern pattern = Pattern.compile("[0-9]{10 }");
            Matcher matcher = pattern.matcher(number);
            if (!matcher.matches()) {
                return false;
            }
        }
        return true;
    }
}
