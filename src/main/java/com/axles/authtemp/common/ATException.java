package com.axles.authtemp.common;

public abstract class ATException extends Exception {
    public ATException() {}

    public ATException(String m) {
        super(m);
    }
}
