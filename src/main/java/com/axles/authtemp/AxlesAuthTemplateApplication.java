package com.axles.authtemp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AxlesAuthTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(AxlesAuthTemplateApplication.class, args);
	}

}
