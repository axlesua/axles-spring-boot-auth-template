package com.axles.authtemp.users;

import com.axles.authtemp.common.ATException;
import com.axles.authtemp.users.data.*;
import com.fitstar.fitstarapi.users.data.*;
import com.axles.authtemp.users.data.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;

public interface UsersService extends UserDetailsService {
    User save(UserDto user) throws UserServiceUserAlreadyExistsException, UserServiceUserDataInvalidException;
    TokenDto auth(AuthDto user) throws UserServiceUserNotFoundException;
    void resetPassword(ResetPasswordDto resetPasswordDto) throws UserServiceUserNotFoundException;
    void edit(EditUserDto userDto, Long id) throws UserServiceUserNotFoundException, UserServiceUserDataInvalidException;
    User uploadImage(Long id, MultipartFile file) throws UserServiceUserNotFoundException, UserServiceUserImageUploadException;
    void changePassword(User user, ChangePasswordDto changePasswordDto) throws UserServiceUserDataInvalidException, UserServiceUserDifferentPasswordException;
    Page<User> getRegisterUser(String firstName, String lastName, Pageable pageable);

    class UserServiceUserAlreadyExistsException extends ATException {}
    class UserServiceUserNotFoundException extends ATException {}
    class UserServiceUserDataInvalidException extends ATException {}
    class UserServiceUserImageUploadException extends ATException {}
    class UserServiceUserIncorrectRoleException extends ATException {}
    class UserServiceUserDifferentPasswordException extends ATException {}
}
