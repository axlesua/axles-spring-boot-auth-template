package com.axles.authtemp.users;

import com.axles.authtemp.common.ATErrorDto;
import com.axles.authtemp.common.BaseController;
import com.axles.authtemp.users.data.ChangePasswordDto;
import com.axles.authtemp.users.data.EditUserDto;
import com.axles.authtemp.users.data.ResetPasswordDto;
import com.axles.authtemp.users.data.UserDto;
import com.axles.authtemp.users.data.entity.Role;
import com.axles.authtemp.users.data.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("/users")
public class UsersController extends BaseController {
    @Autowired
    private UsersService userService;

    /**
     * @api {post} /users Register new user
     * @apiName Registration
     * @apiGroup Users
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "firstName":"name",
     *         "lastName":"lastname",
     *         "username":"omel@gmail.com",
     *         "password":"00000000",
     *      }
     *
     * @apiErrorExample Error-Response:
     *       HTTP/1.1 400 Bad request
     *       {
     *            "error": "BAD_REQUEST",
     *            "code": 400
     *      }
     */
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity register(@RequestBody UserDto userDto) {
        try {
            User created = userService.save(userDto);
        } catch (UsersService.UserServiceUserAlreadyExistsException e) {
            log.debug("Registration failed for user '{}'", userDto.getUsername());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Email already exists"), HttpStatus.BAD_REQUEST);
        } catch (UsersService.UserServiceUserDataInvalidException e) {
            log.debug("Registration failed for user '{}'", userDto.getUsername());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Invalid user data"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * @api {post} /users/reset Recover user's password
     * @apiName Recover password
     * @apiGroup Users
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "username":"omel@gmail.com"
     *      }
     *
     * @apiErrorExample Error-Response:
     *       HTTP/1.1 400 Bad request
     *       {
     *            "error": "BAD_REQUEST",
     *            "code": 400
     *      }
     */
    @RequestMapping(path = "/reset", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity reset(@RequestBody ResetPasswordDto resetPasswordDto) {
        try {
            userService.resetPassword(resetPasswordDto);
        } catch (UsersService.UserServiceUserNotFoundException e) {
            log.debug("Registration failed for user '{}'", resetPasswordDto.getUsername());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "User not found"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    /**
     * @api {patch} /users Edit user
     * @apiDescription All user can do this action
     * @apiName EditUser
     * @apiGroup Users
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "firstName":"name",
     *         "lastName":"lastname",
     *      }
     *
     * @apiErrorExample Error-Response:
     *       HTTP/1.1 400 Bad request
     *       {
     *            "error": "BAD_REQUEST",
     *            "code": 400
     *      }
     */
    @RequestMapping(method = RequestMethod.PATCH)
    @Secured({Role.SUPER_ADMIN, Role.USER})
    public @ResponseBody ResponseEntity editUser(@RequestBody EditUserDto userDto) {
        Long id = getAuthenticatedUser().getId();
        try {
            userService.edit(userDto, id);
        } catch (UsersService.UserServiceUserNotFoundException e) {
            log.debug("Registration failed for user '{}'", id);
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "User not found"), HttpStatus.BAD_REQUEST);
        } catch (UsersService.UserServiceUserDataInvalidException e) {
            log.debug("Editing failed for user '{}'", id);
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Invalid user data"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * @api {post} /users/image Upload user avatar
     * @apiDescription Multipart upload, key name is 'file', All user can do this action.
     * @apiGroup Users
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiError Unauthorized The token of the User is invalid.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad request
     *     {
     *      "error": "BAD_REQUEST",
     *      "code": 400
     *     }
     */
    @RequestMapping(value = "/image", method = RequestMethod.POST)
    @Secured({Role.SUPER_ADMIN, Role.USER})
    public @ResponseBody
    ResponseEntity uploadImage(@RequestPart(value = "file") MultipartFile file) {
        Long id = getAuthenticatedUser().getId();
        User user = null;
        try {
            user = userService.uploadImage(id, file);
        } catch (UsersService.UserServiceUserNotFoundException e) {
            log.debug("Image upload failed for user: not found " + id);
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "User not found"), HttpStatus.BAD_REQUEST);
        } catch (UsersService.UserServiceUserImageUploadException e) {
            log.debug("Image upload failed for user: storage " + id);
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Aws error"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    /**
     * @api {post} /users/logout Logout, delete device
     * @apiGroup Users
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiError Unauthorized The token of the User is invalid
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad request
     *     {
     *      "error": "BAD_REQUEST",
     *      "code": 400
     *     }
     */
    @RequestMapping(value = "/logout", method = RequestMethod.POST)
    @Secured({Role.SUPER_ADMIN, Role.USER})
    public @ResponseBody
    ResponseEntity logout() {
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * @api {get} /users/me Me
     * @apiDescription All registered users can do it
     * @apiGroup Users
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiError Unauthorized The token of the User is invalid.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad request
     *     {
     *      "error": "BAD_REQUEST",
     *      "code": 400
     *     }
     */
    @RequestMapping(value = "/me", method = RequestMethod.GET)
    @Secured({Role.SUPER_ADMIN, Role.USER})
    public @ResponseBody
    ResponseEntity userInformation() {
        User user = getAuthenticatedUser();
        return new ResponseEntity(user, HttpStatus.OK);
    }

    /**
     * @api {patch} /users/password Change password
     * @apiGroup Users
     * @apiDescription All registered users can do it
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiError Unauthorized The token of the User is invalid
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "oldPassword":"oldPassword"
     *         "newPassword":"newPassword"
     *      }
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad request
     *     {
     *      "error": "BAD_REQUEST",
     *      "code": 400
     *     }
     */
    @RequestMapping(value = "/password", method = RequestMethod.PATCH)
    @Secured({Role.SUPER_ADMIN, Role.USER})
    public @ResponseBody
    ResponseEntity changePassword(@RequestBody ChangePasswordDto changePasswordDto) {
        User user = getAuthenticatedUser();
        try {
            userService.changePassword(user, changePasswordDto);
        } catch (UsersService.UserServiceUserDataInvalidException e) {
            log.debug("Changing password failed for user '{}'", user.getId());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Invalid password data"), HttpStatus.BAD_REQUEST);
        } catch (UsersService.UserServiceUserDifferentPasswordException e) {
            log.debug("Changing password failed for user '{}'", user.getId());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Incorrect old password"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * @api {get} /users/register Get register user
     * @apiDescription Super admin can do it
     * @apiGroup Users
     *
     * @apiHeaderExample {json} Headers-Example:
     *   { "Authorization": "token" }
     *
     * @apiError Unauthorized The token of the User is invalid.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 400 Bad request
     *     {
     *      "error": "BAD_REQUEST",
     *      "code": 400
     *     }
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    @Secured({Role.SUPER_ADMIN})
    public @ResponseBody
    ResponseEntity getRegisterUser(@RequestParam(required = false) String filter,  Pageable pageable) {
        Page<User> users = userService.getRegisterUser(filter, filter, pageable);
        return new ResponseEntity(users.getContent(), HttpStatus.OK);
    }

}
