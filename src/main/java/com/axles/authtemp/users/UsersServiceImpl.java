package com.axles.authtemp.users;

import com.amazonaws.SdkClientException;
import com.axles.authtemp.common.AmazonService;
import com.axles.authtemp.common.EmailService;
import com.axles.authtemp.common.ValidationUtils;
import com.axles.authtemp.security.TokenService;
import com.axles.authtemp.users.data.*;
import com.fitstar.fitstarapi.users.data.*;
import com.axles.authtemp.users.data.entity.RegistrationSource;
import com.axles.authtemp.users.data.entity.Role;
import com.axles.authtemp.users.data.entity.User;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private TokenService tokenService;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private UsersRepository userRepository;
    @Autowired
    private BCryptPasswordEncoder cryptPasswordEncoder;
    @Autowired
    private EmailService emailService;
    @Autowired
    private AmazonService amazonService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username);
    }

    @Override
    public User save(UserDto userDto) throws UserServiceUserAlreadyExistsException, UserServiceUserDataInvalidException {
        User user = baseCreateUser(userDto);
        user.setRole(Role.USER);
        return userRepository.save(user);
    }

    @Override
    public void changePassword(User user, ChangePasswordDto changePasswordDto)throws UserServiceUserDataInvalidException, UserServiceUserDifferentPasswordException {
        if (!cryptPasswordEncoder.matches(changePasswordDto.getOldPassword(), user.getPassword())) {
            throw new UserServiceUserDifferentPasswordException();
        }
        if (!ValidationUtils.isValidPassword(changePasswordDto.getNewPassword())) {
            throw new UserServiceUserDataInvalidException();
        }
        user.setPassword(cryptPasswordEncoder.encode(changePasswordDto.getNewPassword()));
        userRepository.save(user);
    }

    @Override
    public Page<User> getRegisterUser(String firstName, String lastName, Pageable pageable) {
        if (lastName != null || firstName != null) {
//            return userRepository.findRegisterUser(Role.USER, firstName, lastName, pageable);
        } else {
//            return userRepository.findAllByRole(Role.USER, pageable);
        }
        return null;
    }

    @Override
    public TokenDto auth(AuthDto authDto) throws UserServiceUserNotFoundException {
        return generateAuth(authDto);
    }

    @Override
    public void resetPassword(ResetPasswordDto resetPasswordDto) throws UserServiceUserNotFoundException {
        User existing = userRepository.findByUsername(resetPasswordDto.getUsername());
        if (existing == null) {
            throw new UserServiceUserNotFoundException();
        }
        String generatedString = generatePassword();
        String encoded = cryptPasswordEncoder.encode(generatedString);
        existing.setPassword(encoded);
        userRepository.save(existing);
        emailService.sendResetPasswordEmail(resetPasswordDto.getUsername(), generatedString);
    }

    @Override
    public void edit(EditUserDto userDto, Long id) throws UserServiceUserNotFoundException, UserServiceUserDataInvalidException {
        User user = userRepository.findById(id).orElseThrow(() -> new UserServiceUserNotFoundException());
        if (userDto.getFirstName() != null) {
            if (!ValidationUtils.isValidFirstName(userDto.getFirstName())) {
                throw new UserServiceUserDataInvalidException();
            }
            user.setFirstName(userDto.getFirstName());
        }
        if (userDto.getLastName() != null) {
            if (!ValidationUtils.isValidLastName(userDto.getLastName())) {
                throw new UserServiceUserDataInvalidException();
            }
            user.setLastName(userDto.getLastName());
        }
        userRepository.save(user);
    }

    @Override
    public User uploadImage(Long id, MultipartFile file) throws UserServiceUserNotFoundException, UserServiceUserImageUploadException {
        if (userRepository.existsById(id)) {
            final String url;
            try {
                url = amazonService.uploadFile(file, "userAvatar", String.valueOf(id));
            } catch (IOException e) {
                e.printStackTrace();
                throw new UserServiceUserImageUploadException();
            } catch (SdkClientException e) {
                e.printStackTrace();
                throw new UserServiceUserImageUploadException();
            }
            Optional<User> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                String timestamp = "?t=" + System.currentTimeMillis();
                user.setImageUrl(url + timestamp);
                return userRepository.save(user);
            } else {
                throw new UserServiceUserNotFoundException();
            }
        } else {
            throw new UserServiceUserNotFoundException();
        }
    }

    private TokenDto generateAuth(AuthDto authDto) throws UserServiceUserNotFoundException {
        TokenDto tokenDto = null;
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(authDto.getUsername(), authDto.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            UserDetails userDetails = userDetailsService.loadUserByUsername(authDto.getUsername());
            String token = tokenService.generateToken(userDetails);
            tokenDto = new TokenDto(token);
        } catch (AuthenticationException e) {
            throw new UserServiceUserNotFoundException();
        }
        return tokenDto;
    }

    private String generatePassword() {
        return RandomStringUtils.random(15, 65, 90, true, true);
    }

    private User baseCreateUser(UserDto userDto) throws UserServiceUserDataInvalidException, UserServiceUserAlreadyExistsException {
        if (!ValidationUtils.isValidUserDto(userDto)) {
            throw new UserServiceUserDataInvalidException();
        }
        User existing = userRepository.findByUsername(userDto.getUsername());
        if (existing != null) {
            throw new UserServiceUserAlreadyExistsException();
        }
        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword(cryptPasswordEncoder.encode(userDto.getPassword()));
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setRegistrationSource(RegistrationSource.EMAIL);
        return user;
    }
}
