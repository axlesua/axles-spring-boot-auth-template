package com.axles.authtemp.users.auth;

import com.axles.authtemp.users.UsersService;
import com.axles.authtemp.users.data.TokenDto;
import com.axles.authtemp.common.ATErrorDto;
import com.axles.authtemp.users.data.AuthDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping("users/auth")
public class AuthController {
    @Autowired
    private UsersService userService;

    /**
     * @api {post} /users/auth Login via email
     * @apiName Login via email
     * @apiGroup Users
     *
     * @apiParamExample {json} Request-Example:
     *     {
     *         "username":"omel@gmail.com",
     *         "password":"00000000",
     *      }
     *
     * @apiErrorExample Error-Response:
     *       HTTP/1.1 401 UNAUTHORIZED
     *       {
     *            "error": "UNAUTHORIZED",
     *            "code": 401
     *      }
     */
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity authenticate(@RequestBody AuthDto authDto) {
        TokenDto token = null;
        try {
            token = userService.auth(authDto);
        } catch (UsersService.UserServiceUserNotFoundException e) {
            log.debug("Login failed for user '{}'", authDto.getUsername());
            return new ResponseEntity<>(new ATErrorDto(HttpStatus.BAD_REQUEST.value(), "Invalid credentials"), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(token, HttpStatus.OK);
    }

}

