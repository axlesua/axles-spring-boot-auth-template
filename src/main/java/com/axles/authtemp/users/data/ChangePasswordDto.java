package com.axles.authtemp.users.data;

import lombok.Getter;

@Getter
public class ChangePasswordDto {
    private String oldPassword;
    private String newPassword;
}
