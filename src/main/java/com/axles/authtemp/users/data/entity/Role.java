package com.axles.authtemp.users.data.entity;

public interface Role {
    String USER = "ROLE_USER";
    String SUPER_ADMIN = "ROLE_SUPER_ADMIN";

}
