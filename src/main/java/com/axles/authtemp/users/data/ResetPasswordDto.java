package com.axles.authtemp.users.data;

import lombok.Getter;

@Getter
public class ResetPasswordDto {
    private String username;
}
