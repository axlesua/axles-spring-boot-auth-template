package com.axles.authtemp.users.data;

import com.axles.authtemp.users.data.entity.User;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class UserResponce {
    private List<User> users;
    private long total;

    public UserResponce(List<User> users, long total) {
        this.users = users;
        this.total = total;
    }
}
