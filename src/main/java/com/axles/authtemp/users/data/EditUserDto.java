package com.axles.authtemp.users.data;

import lombok.Getter;

@Getter
public class EditUserDto {
    private String firstName;
    private String lastName;
    private String company;
    private String address;
    private String phoneNumber;
}
