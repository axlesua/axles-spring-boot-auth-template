package com.axles.authtemp.users.data.entity;

public enum RegistrationSource {
    EMAIL("EMAIL");

    private String name;

    RegistrationSource(String name) {
        this.name = name;
    }
}
