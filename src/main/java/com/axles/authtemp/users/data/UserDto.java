package com.axles.authtemp.users.data;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Pattern;

@Getter
@Setter
public class UserDto {
    private String firstName;
    private String lastName;
    private String username;
    private String password;
    private String role;
    private String imageUrl;
    private String company;
    private String address;
    private String phoneNumber;
}
