package com.axles.authtemp.users.data;

import lombok.Getter;

@Getter
public class AuthDto {
    private String username;
    private String password;
}
